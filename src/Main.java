import com.zuitt.example.Contact;
import com.zuitt.example.Phonebook;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
//        System.out.println("Hello world!");

        Phonebook phonebook = new Phonebook();

        Contact contact1 = new Contact("Harold Jamin", "09198037676", "QC");
        Contact contact2 = new Contact("Loise Labrador", "09353975233", "QC");

        phonebook.setContacts(contact1);
        phonebook.setContacts(contact2);

        if(phonebook.getContacts().isEmpty()){
            System.out.println("Phonebook has no contacts.");
        } else{
            for(Contact c: phonebook.getContacts()){
                printInfo(c);
            }
        }

    }

    public static void printInfo(Contact contact) {
        System.out.println(contact.getName());
        System.out.println();

        if(contact.getContactNumber() != null) {
            System.out.println(contact.getName() + "'s contact number is: " + contact.getContactNumber());
        }
        else {
            System.out.println(contact.getName() + " has no registered number.");
        }

        if(contact.getAddress() != null) {
            System.out.println(contact.getName() + "'s address is: " + contact.getAddress());
            System.out.println();
        }
        else {
            System.out.println(contact.getName() + " has no registered address.");

        }
    }

}
